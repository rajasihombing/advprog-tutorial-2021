package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.*;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    ArrayList<Spell> lstSpell;

    public ChainSpell(ArrayList<Spell> lst){
        lstSpell = lst;
    }

    @Override
    public void cast() {
        for(Spell spl:lstSpell){
            spl.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = lstSpell.size()-1;i>=0;i--){
            lstSpell.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
