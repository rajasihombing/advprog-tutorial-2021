package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

public class CaesarTransformation extends Transform{
    private int shift;

    public CaesarTransformation(int n){
        while(n > 26){
            n -= 26;
        }
        this.shift = n;
    }

    public CaesarTransformation(){
        this.shift = 5;
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        int codexSize = codex.getCharSize();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            char oldCharacter = text.charAt(i);
            int charIdx = codex.getIndex(oldCharacter);
            int newCharIdx = charIdx + selector * this.shift;
            newCharIdx = newCharIdx < 0? codexSize +  newCharIdx : newCharIdx % codexSize;
            char newCharacter = codex.getChar(newCharIdx);
            res[i] = newCharacter;

        }
        return new Spell(new String(res), codex);
    }



}
