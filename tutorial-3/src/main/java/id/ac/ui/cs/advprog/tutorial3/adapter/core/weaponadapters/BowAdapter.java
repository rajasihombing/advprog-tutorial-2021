package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;


// TODO: complete me :)
public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow){
        this.bow = bow;
        this.isAimShot = true;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        if(isAimShot){
            this.isAimShot = false;
            return "Aim Shot Mode";
        } else {
            this.isAimShot = true;
            return "Spontan Mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
