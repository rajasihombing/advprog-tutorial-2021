package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean strong_spell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        strong_spell = false;
    }

    @Override
    public String normalAttack() {
        strong_spell = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!strong_spell) {
            this.strong_spell = true;
            return spellbook.largeSpell();
        } else {
            this.strong_spell = false;
            return "Spell Overheat!!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
