package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;


import java.util.List;
import java.util.ArrayList;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {
    private List<Weapon> weapons = new ArrayList<>();

    // feel free to include more repositories if you think it might help :)
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if(weapons.isEmpty()) {
            weapons.addAll(weaponRepository.findAll());
            for (Spellbook spellBooks: spellbookRepository.findAll()) {
                SpellbookAdapter spellBook = new SpellbookAdapter(spellBooks);
                Weapon weapon = (Weapon) spellBook;
                weaponRepository.save(weapon);
                weapons.add(weapon);
            }
            for (Bow bows: bowRepository.findAll()) {
                BowAdapter bow = new BowAdapter(bows);
                Weapon weapon = (Weapon) bow;
                weaponRepository.save(weapon);
                weapons.add(weapon);
            }
        }
        return weapons;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(attackType == 0) {
            logRepository.addLog(String.format("%s attacked with %s (normal attack): %s", weapon.getHolderName(), weapon.getName(), weapon.normalAttack()));
        } else {
            logRepository.addLog(String.format("%s attacked with %s (charged attack): %s", weapon.getHolderName(), weapon.getName(), weapon.chargedAttack()));
        }
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }

}
