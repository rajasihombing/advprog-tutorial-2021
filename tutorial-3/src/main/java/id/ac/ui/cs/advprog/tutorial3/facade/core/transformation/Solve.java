package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.List;

public class Solve {
    private final List<Transform> query;

    public Solve(){
        this.query = new ArrayList<Transform>();
        this.query.add(new AbyssalTransformation());
        this.query.add(new CelestialTransformation());
        this.query.add(new CaesarTransformation());
    }

    public Solve(String ce, int ab, int ca){
        this.query = new ArrayList<Transform>();
        this.query.add(new AbyssalTransformation(ab));
        this.query.add(new CelestialTransformation(ce));
        this.query.add(new CaesarTransformation(ca));
    }

    public String encode(String text){
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        for(Transform transformation: query){
            spell = transformation.encode(spell);
        }
        Spell result = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return result.getText();
    }

    public String decode(String text){
        Spell spell = new Spell(text, RunicCodex.getInstance());
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        for(int i = query.size()-1; i>=0; i--){
            spell = query.get(i).decode(spell);
        }
        return spell.getText();

    }
}

