package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CaesarTransformationTest {
    private Class<?> caesarCipherClass;

    @BeforeEach
    public void setup() throws Exception{
        caesarCipherClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation");
    }

    @Test
    public void testCaesarCipherHasEncodeMethod() throws Exception {
        Method translate = caesarCipherClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarCipherEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";

        Spell result = new CaesarTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarEncodesWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "fnsv4nMn0qMVM9r06M61MnMoynpx5zv6uM61Ms14trM174M5914q";

        Spell result = new CaesarTransformation(13).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarDecode() throws Exception {
        Method translate = caesarCipherClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarDecodesCorrectly() throws Exception {
        String text = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new CaesarTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarDecodesWithCustomKey() throws Exception {
        String text = "fnsv4nMn0qMVM9r06M61MnMoynpx5zv6uM61Ms14trM174M5914q";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new CaesarTransformation(13).decode(spell);
        assertEquals(expected, result.getText());
    }




}
