package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SolveTest {
    private Class<?> solveClass;
    private Solve solve, solverWithCustomKey;

    @BeforeEach
    public void setup() throws Exception{
        solveClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Solve");
        solve = new Solve();
        solverWithCustomKey = new Solve("iamtheboneofmysword", 5,5);
    }

    @Test
    public void solverHasEncodeMethodTest() throws Exception{
        Method encode = solveClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, encode.getParameterCount());
        assertEquals("java.lang.String", encode.getGenericReturnType().getTypeName());
    }

    @Test
    public void solverHasDecodeMethodTest() throws Exception{
        Method decode = solveClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, decode.getParameterCount());
        assertEquals("java.lang.String", decode.getGenericReturnType().getTypeName());
    }

    @Test
    public void testEncodeResult(){
        String result = solve.encode("Safira and I went to a blacksmith to forge our sword");
        assertEquals("%!%|CKvG))M<B:)JKy$BN_i%F:Mwa$aM[|[!|C/G%iBFFHD.&$*L", result);
    }

    @Test
    public void testDecodeResult(){
        String result = solve.decode("%!%|CKvG))M<B:)JKy$BN_i%F:Mwa$aM[|[!|C/G%iBFFHD.&$*L");
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }

    @Test
    public void testEncodeWithCustomKeyResult(){
        String result = solverWithCustomKey.encode("Safira and I went to a blacksmith to forge our sword");
        assertEquals("%!%|CKvG))M<B:)JKy$BN_i%F:Mwa$aM[|[!|C/G%iBFFHD.&$*L", result);
    }

    @Test
    public void testDecodeWithCustomKeyResult(){
        String result = solverWithCustomKey.decode("%!%|CKvG))M<B:)JKy$BN_i%F:Mwa$aM[|[!|C/G%iBFFHD.&$*L");
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }



}
